class PlayerController extends MonoBehaviour
{	
	var inEditorSpeed : float = 1;
	var useAccelerometer : boolean = true;
	var useTouch : boolean = true;
	var accelerometerSpeed : float = 1;
	
	private var hasTwoPlayers : boolean = false;
	
	private var accelAxis : int = 0;
	private var touchAxis : int = 0;
	
	var calibrationModifier : float = 0;
	
	private var accelCalib : Vector3 = Vector3.zero;
	private var smooth : float;
	private var touchSmooth: float;
	var touchSpeed : float = 0.3;
	private var touchVelocity : float;
	private var touchVelocity2 : float;
	
	private var player1 : Transform;
	var player2 : Transform;
	
	var cam : Camera;
	function Awake()
	{
		//read pref settings
		var settings : SettingsInfo = new SettingsInfo();
		settings.Load();
		
		accelerometerSpeed = settings.accelerometerSensitivity;
		accelAxis          = settings.accelerometerAxis;
		touchSpeed 		   = settings.touchSensitivity;
		touchAxis		   = settings.touchAxis;
		
		useAccelerometer   = settings.enableAccelerometer;
		useTouch           = settings.enableTouch;
		

		var numPlayers : int = PlayerPrefs.GetInt("numPlayers");
		Debug.Log("PlayerController: " + numPlayers + " players");
		if (numPlayers > 1)
		{
			hasTwoPlayers = true;
			useAccelerometer = false;
			useTouch = true;
		}
	}
	
	function Start() 
	{
		accelCalib.x = iPhoneInput.acceleration.x; //+ calibrationModifier;
		accelCalib.y = iPhoneInput.acceleration.y;
		
		cam = gameObject.FindWithTag("MainCamera").GetComponent("Camera");
		player1 = transform;
	}
	
	function Update () {
		
		
		//iphone accelerometer
		if (useAccelerometer)
		{
			
			var accelerator : Vector3 = iPhoneInput.acceleration;
			if (accelAxis == 0)
				smooth = Mathf.Lerp(smooth, (accelerator.x - accelCalib.x) * accelerometerSpeed * Time.deltaTime,  Time.time);
			else
				smooth = Mathf.Lerp(smooth, (accelerator.y - accelCalib.y) * accelerometerSpeed * Time.deltaTime, Time.time);
			player1.Translate(0, smooth, 0);
			//quick recalib using the keyboard for resting
			if (Application.isEditor)
			{
				if (Input.GetKey("space"))
				{	
						Debug.Log("PlayerController: Recalibrating accelerometer : iphone accel" + 
							iPhoneInput.acceleration.x +" mod: "+ calibrationModifier);
						accelCalib.x = iPhoneInput.acceleration.x + calibrationModifier;
				}
			}
		}
		
		//iphone touch/mouse
		if (useTouch)
		{
			for (var touch : iPhoneTouch in iPhoneInput.touches)
			{
					var p : Vector3 = cam.ScreenToWorldPoint (Vector3 (touch.position.x,touch.position.y,15));
			      	
					if (p.x < 0)
						player1.position.y = Mathf.SmoothDamp(player1.position.y, p.y, touchVelocity, touchSpeed);
					else
					{
						if (hasTwoPlayers)
							player2.position.y = Mathf.SmoothDamp(player2.position.y, p.y, touchVelocity2, touchSpeed);
					}
			}

		}
		
		//handle keyboard for testing
		if (Application.isEditor)
		{
			var translation : float = Input.GetAxis ("Vertical") * inEditorSpeed;
			player1.Translate (0, translation, 0);
			
			if (Input.GetMouseButton(0)) 
			{
				var p2 : Vector3 = cam.ScreenToWorldPoint (Vector3 (Input.mousePosition.x,Input.mousePosition.y,15));
		      	player1.position.y = Mathf.SmoothDamp(player1.position.y, p2.y, touchVelocity, touchSpeed);
			}
			
		}
	}
}
@script AddComponentMenu("Controllers/Player Controller")