class AIPlayer extends MonoBehaviour
{
	var ball : Transform;
	var followSpeed : float = 0.3;
	private var yVelocity = 0.0;
	
	function Awake()
	{
		var settings : SettingsInfo = new SettingsInfo();
		settings.Load();
		followSpeed = settings.aiSpeed; 
	}
	function Update () {
		FollowBall();
	}
	
	function FollowBall()
	{
		//transform.position.y =  Mathf.Lerp(transform.position.y, ball.position.y, Time.time);
		transform.position.y = Mathf.SmoothDamp(transform.position.y, ball.position.y, yVelocity, followSpeed);
	}

}
