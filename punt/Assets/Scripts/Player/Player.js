class Player extends MonoBehaviour
{
	var upperLimit : float = 11.5;
	var lowerLimit : float = -10;

	var scoreboard : GUIText;
	
	private var score : int = 0;
	
	private var myTransform : Transform;
	function Start()
	{
		myTransform = transform;
		scoreboard.text = ""+score;
	}
	function Update()
	{
		myTransform.position.y = Mathf.Clamp(myTransform.position.y, lowerLimit, upperLimit);
	}

	function GetScore() : int
	{
		return score;
	}
	
	function AddPoint()
	{
		score++;
		scoreboard.text = ""+score;
	}
}
