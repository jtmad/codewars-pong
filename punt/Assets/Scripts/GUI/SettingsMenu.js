@script ExecuteInEditMode()

var boardWidth : int = 400;
var boardHeight : int = 400;
var boardLeft : int = 10;
var boardTop : int = 70;

var testMode : boolean = false;

var guiskin : GUISkin;
var audioclip : AudioClip;

private var settings : SettingsInfo;
private var tmp:TextEditor = new TextEditor();

private var axisStrings : String[] = ["Horizontal", "Vertical"];
private var optionStrings : String[] = ["Player", "Controls", "Game", "Audio"];
private var screenOption : int;

function Awake()
{
	useGUILayout = false;
}

function Start()
{
	settings = new SettingsInfo();
	settings.Load();
}

function OnGUI () {
	GUI.skin = guiskin;
	
	/*heading*/
	GUI.Label(Rect (30, 20, 400, 100), "Settings");
	
	GUI.BeginGroup (Rect (boardLeft,boardTop, boardWidth, boardHeight));
	
	switch (screenOption)
	{
		case 0:
			GUIPlayerSettings();
			break;
		case 1:
			GUIControls();
			break;
		case 2:
			GUIGameOptions();
			break;
		case 3:
			GUIAudioOptions();
			break;
	}

	
	GUI.EndGroup();
	
	GUIMainOptionBar();
	GUISaveOrCancel();
}

function GUIPlayerSettings()
{	
	//UI Difficulty
	GUI.Label(Rect(5, 0, 180, 30), "AI Speed");
	settings.aiSpeed = 
			GUI.HorizontalSlider (Rect (190, 5, 100, 30), settings.aiSpeed, 0.8, 0.01);
			
	GUI.Label(Rect(5, 40, 180, 30), "Paddle Size");
	settings.paddleSize =
	 		GUI.HorizontalSlider (Rect (200, 35, 100, 30), settings.paddleSize, 0.8, 6.01);
	
}
function GUIControls()
{
	GUI.Label(Rect(5, -10, 180, 65), "Accelerometer Sensitivity");
	settings.accelerometerSensitivity = 
			GUI.HorizontalSlider (Rect (190, 5, 100, 30), settings.accelerometerSensitivity, 0.0, 5.5);
			
	GUI.Label(Rect(5, 40, 180, 30), "Accelerometer Axis");
	settings.accelerometerAxis = GUI.Toolbar (Rect (170, 35, 170, 30), settings.accelerometerAxis, axisStrings);
			
	GUI.Label(Rect(5, 70, 180, 30), "Touch Sensitivity");
	settings.touchSensitivity =
				GUI.HorizontalSlider (Rect (190, 75, 100, 30), settings.touchSensitivity, 0.5, 0.001);
	
	//GUI.Label(Rect(5, 110, 180, 30 ), "Touch Axis");			
	//settings.touchAxis = GUI.Toolbar (Rect (170, 105, 170, 30), settings.touchAxis, axisStrings);
	
	GUI.Label(Rect(5, 150, 180, 30), "Accelerometer Enabled");
	settings.enableAccelerometer = GUI.Toggle (Rect (190, 150, 28, 30), settings.enableAccelerometer, "");
	GUI.Label(Rect(5, 180, 180, 30), "Touch Enabled");
	settings.enableTouch = GUI.Toggle (Rect (190, 180, 28, 30), settings.enableTouch, "");
}

function GUIGameOptions()
{
	GUI.Label(Rect(5, 0, 180, 30), "Points needed to win");
	settings.pointsToWin = 
			GUI.HorizontalSlider (Rect (200, 5, 100, 30), settings.pointsToWin, 1, 20);
	GUI.Label(Rect(335, 0, 180, 30), "" + settings.pointsToWin);
	
	GUI.Label(Rect(5, 40, 180, 30), "Speed of Ball");
	settings.ballSpeed = 
			GUI.HorizontalSlider (Rect (200, 35, 140, 30), settings.ballSpeed, 1.0, 50.0);


}

function GUIAudioOptions()
{
	GUI.Label(Rect(5, 40, 180, 30), "Sound Enabled");
	settings.enableSound = GUI.Toggle (Rect (200, 45, 130, 30), settings.enableSound, "?");
	
	//GUI.Label(Rect(5, 70, 180, 30), "Music Enabled");
	//settings.enableMusic = GUI.Toggle (Rect (200, 75, 100, 30), settings.enableMusic, "?");
}

function GUIMainOptionBar()
{
	screenOption = GUI.SelectionGrid (Rect (378, 165, 95, 150), screenOption, optionStrings,1);
}

function GUISaveOrCancel()
{
	GUI.BeginGroup (Rect (5, 287, 300, 40));
	
	if (GUI.Button( Rect(0, 0, 60, 30), "Cancel")) 
  		Back();
    if (GUI.Button( Rect(65, 0, 60, 30 ), "Save"))
		Save();
	if (GUI.Button( Rect(130, 0, 60, 30), "Reset"))
		settings.ResetAll();
	
	if (testMode)
	{
		if (GUI.Button( Rect(195, 0, 60, 30), "Debug"))
		{
			Debug.Log("SettingsMenu: Debug: AI Speed: " + settings.aiSpeed);
		}
	}
	GUI.EndGroup();
}

function Back()
{
	var main : Behaviour = GetComponent("MainMenu");
	main.enabled = true;
	
	var scores : Behaviour = GetComponent("SettingsMenu");
	scores.enabled = false;	
}

function Save()
{
	settings.Save();
	Back();
}