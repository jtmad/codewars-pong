@script ExecuteInEditMode()


var backdrop : Texture2D;
var skin : GUISkin;

var audioclip : AudioClip;

var horizontalOrientation : boolean = true;

var info : GameInfo;
var heading : GUIText;
private var isLoading : boolean = false;
private var tmp:TextEditor = new TextEditor();

function Awake () { 
   // set screen orientation and sleep mode 
   useGUILayout = false;
   screenCanDarken = true; 

	useGUILayout = false;
	if (horizontalOrientation)
		Screen.SetResolution(480, 320, true);
	else
		Screen.SetResolution(320, 480, true);
	
	audio.clip = audioclip;
		
}

function OnDisable()
{
	if (heading != null)
		heading.enabled = false;
}
function OnEnable()
{
	if (heading != null)
		heading.enabled = true;
}

function OnGUI()
{
	GUI.skin = skin;
	
	/* Background Image */
	var backgroundStyle : GUIStyle = new GUIStyle(); 
	backgroundStyle.normal.background = backdrop;
	GUI.Label ( Rect((Screen.width - (Screen.height *2)) * 0.75, 0, Screen.height * 2, Screen.height), "", backgroundStyle);
	
	/* Start Button */
	if (GUI.Button( Rect( 120, Screen.height - 160, 250, 50), "Player vs CPU")) 
	{
		//audio.PlayOneShot(audioclip);
  		StartGame(1);
 	}
	if (GUI.Button( Rect( 120, Screen.height - 107, 250, 50), "Player vs Player"))
	{
		//audio.PlayOneShot(audioclip);
		StartGame(2);
	}

	/* High Scores Button */
	if (GUI.Button( Rect( (Screen.width/2)-95, Screen.height - 55, 200, 40), "Settings")) 
	{
		//audio.PlayOneShot(audioclip);
 		Settings();
	}
	
}

function StartGame(players : int)
{
	isLoading = true;
	info.numPlayers = players;
	PlayerPrefs.SetInt("numPlayers", players);
	
	var settings : SettingsInfo = new SettingsInfo();
	settings.Load();
	if (settings.enableAccelerometer && (players == 1))
		Application.LoadLevel("Calibrate");
	else
		Application.LoadLevel("Game");
}

function Settings()
{
	var scores : Behaviour = GetComponent("SettingsMenu");
	scores.enabled = true;
	
	var main : Behaviour = GetComponent("MainMenu");
	main.enabled = false;
}



