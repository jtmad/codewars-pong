class GameController extends MonoBehaviour
{
	var ball : GameObject;
	//var player1 : Player;
	var leftGoal  : float = -20;
	var rightGoal : float =   5;
	var ballStartPosition : Vector3;
	
	var player1 : Player;
	var player2 : Player;
	
	var pointsToWin : int;
	private var isRunning : boolean = true;
	
	private var ballTransform : Transform;
	function Awake()
	{
		Screen.SetResolution(480, 320, true);
		iPhoneSettings.screenCanDarken = false;
		ballStartPosition = ball.transform.position;
		
		//load settings
		var settings : SettingsInfo = new SettingsInfo();
		settings.Load();
		pointsToWin = settings.pointsToWin;
	}
	
	function Start()
	{
		ballTransform = ball.transform;

		var numPlayers : int = PlayerPrefs.GetInt("numPlayers");

		Debug.Log("PlayerController: " + numPlayers + " players");
		if (numPlayers > 1)
		{
			//destroy AI controller
			var ai : AIPlayer = player2.GetComponent("AIPlayer");
			Destroy(ai);
		}
	
	}
	
	function Update () 
	{
		if (isRunning)
		{
			if (ballTransform.position.x < leftGoal)
			{
				player2.AddPoint();
				ResetBall();
			}
		
			if (ballTransform.position.x > rightGoal)
			{
				player1.AddPoint();
				ResetBall();
			}
		
			//check if player won
			if (player1.GetScore() == pointsToWin)
				PlayerWon(player1);
			if (player2.GetScore() == pointsToWin)
				PlayerWon(player2);
		}
	}
	function ResetBall()
	{
		ballTransform.position = ballStartPosition;
		ball.rigidbody.velocity.y = 0;
	}
	
	function PlayerWon(plyr : Player)
	{
		isRunning = false;
		//ball.renderer.enabled = false;
		var b : Ball = ball.GetComponent("Ball");
		b.Stop();
		
		var menu : GameOverMenu = gameObject.GetComponent(GameOverMenu);
		menu.enabled = true;
	}

}
