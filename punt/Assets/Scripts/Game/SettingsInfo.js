import System;
//import System.IO;

class SettingsInfo
{
	var aiSpeed : float;
	var paddleSize : float;
	
	var enableAccelerometer : boolean;
	var accelerometerSensitivity : float;
	var accelerometerAxis : float;
	
	var enableTouch : boolean;
	var touchSensitivity : float;
	var touchAxis : int;
	
	var pointsToWin : int;
	
	var ballSpeed : int;
	
	var enableSound : boolean;
	var enableMusic : boolean;
	
	
	
	//var fileName : String = "settings.dat";
	//private var docPath : String = Application.dataPath;

	function SettingsInfo()
	{ 
		/*set file name / path. use doc path if on the iphone */
	    /*if(!Application.isEditor) 
	    { 
	          docPath = docPath.Substring(0, docPath.Length - 4) + "Documents"; 
	    } 
	    fileName = docPath + "/"+ fileName;
		*/
		
	}
	
	function Load()
	{
		//Debug.Log("SettingsInfo: Load: File: " + fileName);
		aiSpeed                  = PlayerPrefs.GetFloat("aiSpeed", 0.2);
		paddleSize               = PlayerPrefs.GetFloat("paddleSize", 3.0);
		accelerometerSensitivity = PlayerPrefs.GetFloat("accelerometerSensitivity", 3.0);
		accelerometerAxis        = PlayerPrefs.GetInt("accelerometerAxis", 0);
		touchSensitivity         = PlayerPrefs.GetFloat("touchSensitivity", 0.3 );
		touchAxis                = PlayerPrefs.GetInt("touchAxis", 0); 
		pointsToWin              = PlayerPrefs.GetInt("pointsToWin", 10);
		ballSpeed                = PlayerPrefs.GetFloat("ballSpeed", 15.0);
		enableSound				 = Boolean.Parse(PlayerPrefs.GetString("enableSound", "true"));
		enableMusic              = Boolean.Parse(PlayerPrefs.GetString("enableMusic", "true"));
		enableAccelerometer      = Boolean.Parse(PlayerPrefs.GetString("enableAccelerometer", "true"));
		enableTouch              = Boolean.Parse(PlayerPrefs.GetString("enableTouch", "true"));
		
		/*
		if(File.Exists(fileName)) 
	    { 
	       var sr = File.OpenText(fileName); 
	       var line = sr.ReadLine(); 
		   var lineNum : int = 0;
	       while(line != null && (line.length > 0)) 
	       { 
				switch(lineNum)
				{
					case 0:
						aiSpeed = parseFloat(line);
						Debug.Log("SettingsInfo: Load: aiSpeed: " + aiSpeed );
						break;
					case 1:
						accelerometerSensitivity = parseFloat(line);
						Debug.Log("SettingsInfo: Load: accelerometerSensitivity: " + accelerometerSensitivity );
						break;
					case 2:
						accelerometerAxis = parseFloat(line);
						Debug.Log("SettingsInfo: Load: accelerometerAxis: " + accelerometerAxis );
						break;
					case 3:
						touchSensitivity = parseFloat(line);
						Debug.Log("SettingsInfo: Load: touchSensitivity: " + touchSensitivity );
						break;
					case 4:
						touchAxis = parseInt(line);
						Debug.Log("SettingsInfo: Load: touchAxis: " + touchAxis );
						break;
					case 5:
						pointsToWin = parseInt(line);
						Debug.Log("SettingsInfo: Load: pointsToWin: " + pointsToWin );
						break;
					case 6:
						ballSpeed = parseFloat(line);
						Debug.Log("SettingsInfo: Load: ballSpeed: " + ballSpeed);
						break;
					case 7:
						enableSound = Boolean.Parse(line);
						break;
					case 8:
						enableMusic = Boolean.Parse(line);
						break;
					case 9:
						enableAccelerometer = Boolean.Parse(line);
					case 10:
						enableTouch = Boolean.Parse(line);
				}

	         line = sr.ReadLine(); 
			 lineNum++;
	       }    
		   sr.Close();
	    } 
	    else 
	    { 
	       Debug.Log("SettingsInfo No Settings yet: " + fileName );
	 	   ResetAll();
		   Save();
		   Load();
	    } 
	 */
	}
	
	function ResetAll()
	{ 
		PlayerPrefs.DeleteAll();
		Load();
		/*
		accelerometerSensitivity  = 3;
		touchSensitivity          = 0.3;
		aiSpeed                   = 0.2;
		pointsToWin               = 10;
		ballSpeed                 = 15;
		enableSound               = true;
		enableMusic               = true;
		enableAccelerometer       = true;
		enableTouch               = true;
		*/
	}
	
	function Save()
	{
		//Debug.Log("SettingsInfo: Save: File: " + fileName);

		PlayerPrefs.SetFloat("aiSpeed", aiSpeed);
		PlayerPrefs.SetFloat("paddleSize", paddleSize);
		PlayerPrefs.SetFloat("accelerometerSensitivity", accelerometerSensitivity);
		PlayerPrefs.SetInt("accelerometerAxis", accelerometerAxis);
		PlayerPrefs.SetFloat("touchSensitivity", touchSensitivity);
		PlayerPrefs.SetInt("touchAxis", touchAxis);
		PlayerPrefs.SetInt("pointsToWin", pointsToWin);
		PlayerPrefs.SetFloat("ballSpeed", ballSpeed);
		PlayerPrefs.SetString("enableSound", enableSound + "");
		PlayerPrefs.SetString("enableMusic", enableMusic + "");
		PlayerPrefs.SetString("enableAccelerometer", enableAccelerometer + "");
		PlayerPrefs.SetString("enableTouch", enableTouch + "");
		/*
	    var sr = File.CreateText(fileName);
		var id : int = 0;

		sr.WriteLine("" + aiSpeed);
		sr.WriteLine("" + accelerometerSensitivity);
		sr.WriteLine("" + accelerometerAxis);
		sr.WriteLine("" + touchSensitivity);
		sr.WriteLine("" + touchAxis);
		sr.WriteLine("" + pointsToWin);
		sr.WriteLine("" + ballSpeed);
		sr.WriteLine("" + enableSound);
		sr.WriteLine("" + enableMusic);
		sr.WriteLine("" + enableAccelerometer);
		sr.WriteLine("" + enableTouch);
		
	    sr.Close();
		*/
	}
}