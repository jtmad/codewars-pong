class Ball extends MonoBehaviour
{
	var blips : AudioClip[];
	
	private var direction : int;
	private var zClamp : float;
	private var newvelocity : float;
	
	var ballSpeed : float = 15;
	var momentum : float = 200;
	var soundEnabled : boolean = true;
	
	//iphone optimization
	private var myTransform : Transform;
	private var myRigid : Rigidbody;
	
	function Awake()
	{
		//load settings
		var settings : SettingsInfo = new SettingsInfo();
		settings.Load();
		ballSpeed = settings.ballSpeed;
		soundEnabled = settings.enableSound;
		
		myTransform = transform;
		myRigid = rigidbody;
	}
	function Start()
	{
		direction = 0;
		myRigid.AddForce (Vector3.left * 500);
		zClamp = myTransform.position.z;
	}
	
	function Update()
	{
		//clamp z
		myTransform.position.z = Mathf.Clamp(transform.position.y, zClamp, zClamp);
		//keep momentum
		 newvelocity = myRigid.velocity.x * momentum;
		 myRigid.velocity.x = Mathf.Clamp(newvelocity, (ballSpeed * -1 * (Time.deltaTime * 100)), ballSpeed);
	}
	
	function OnCollisionEnter(collision : Collision)
	{
		Debug.Log("Collision");
		if (collision.gameObject.tag == "Paddle")
		{
			direction = direction * -1;
		}
		direction = direction * -1;
		//play audio blip
		if (soundEnabled)
			audio.PlayOneShot(blips[Random.Range(0, 9)]);
	}
	
	function Stop()
	{
		renderer.enabled = false;
		zClamp = 0;
	}
	
}
